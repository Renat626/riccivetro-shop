const sequelize = require("../db");
const {DataTypes} = require("sequelize");

const Orders = sequelize.define("orders", {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    products: {type: DataTypes.STRING(10000), allowNull: true},
    summ: {type: DataTypes.STRING, allowNull: false},
    delivery: {type: DataTypes.STRING, allowNull: false},
    address: {type: DataTypes.STRING, allowNull: true},
    comment: {type: DataTypes.STRING, allowNull: true},
    payment_method: {type: DataTypes.STRING, allowNull: true}
})

module.exports = Orders;