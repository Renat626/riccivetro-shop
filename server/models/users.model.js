const sequelize = require("../db");
const {DataTypes} = require("sequelize");
const Orders = require("./orders.model");

const Users = sequelize.define("users", {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    user_name: {type: DataTypes.STRING, allowNull: true},
    user_phone: {type: DataTypes.STRING, allowNull: false},
    user_email: {type: DataTypes.STRING, allowNull: false},
    user_password: {type: DataTypes.STRING, allowNull: true},
    refresh_token: {type: DataTypes.STRING(10000), allowNull: true},
    user_role: {type: DataTypes.STRING, allowNull: true, defaultValue: "user"}
});

Users.hasMany(Orders,{as: 'Orders', foreignKey: 'user_id'});

module.exports = Users;