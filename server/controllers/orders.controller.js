const OrdersService = require("../services/orders.service");

class OrdersController {
    async create(req, res) {
        const products = req.body.products;
        const summ = req.body.summ;
        const delivery = req.body.delivery;
        const address = req.body.address;
        const comment = req.body.comment;
        const payment_method = req.body.payment_method;
        const name = req.body.name;
        const email = req.body.email;
        const phone = req.body.phone;
        const password = req.body.password;
        console.log(password);

        const answer = await OrdersService.create(products, summ, delivery, address, comment, payment_method, name, email, phone, password);

        if (answer.status == false) {
            res.status(400).json(answer);
        } else {
            res.json(answer);
        }
    }
}

module.exports = new OrdersController();