const express = require("express");
require("dotenv").config();
const process = require('process');
const body_parser = require("body-parser");
const sequelize = require("./db");
const cors = require("cors");
const route = require("./routes/index.route");

const PORT = process.env.PORT || 5000;
const corsOpt = {
    credentials: true,
    origin: "*",
    optionsSuccessStatus: 200
}

const app = express();
app.use(cors(corsOpt));
app.use(body_parser());
app.use("/api", route);

const start = async () => {
    try {
        await sequelize.authenticate();
        await sequelize.sync();
        app.listen(PORT, () => {
            console.log(`Port: ${PORT}`);
            // console.log(process.env.NODE_ENV.trim(), corsOptOrigin);
        })
    } catch (e) {
        console.log(e);
    }
}

start();