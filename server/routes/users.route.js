const Router = require("express");
const router = new Router();
const UsersController = require("../controllers/users.controller");

router.post("/registration", UsersController.registration);
router.post("/login", UsersController.login);
router.get("/check", UsersController.check);
router.get("/logout", UsersController.logout);

module.exports = router;