const Router = require("express");
const route = new Router();
const usersRoute = require("./users.route");
const ordersRoute = require("./orders.route");

route.use("/user", usersRoute);
route.use("/order", ordersRoute);

module.exports = route;