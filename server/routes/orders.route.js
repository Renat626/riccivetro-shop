const Router = require("express");
const router = new Router();
const OrdersController = require("../controllers/orders.controller");

router.post("/create", OrdersController.create);

module.exports = router;