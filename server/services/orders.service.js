const UserService = require("./users.service");
const Orders = require("../models/orders.model");
const TokenService = require("./token.service");

class OrdersService {
    async create(products, summ, delivery, address, comment, payment_method, name, email, phone, password) {
        const return_obj = {
            status: true,
            status_text: "Заказ создан."
        }

        if (password == undefined) {
            password = "";

            function getRandomArbitrary(min, max) {
                return Math.floor(Math.random() * (max - min) + min);
            }
                
            let arr_en = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

            for (let i = 0; i < 10; i++) {
                password += arr_en[getRandomArbitrary(0, arr_en.length - 1)];
            }
        }

        console.log(password);

        let user;

        try {
            user = await UserService.registration(name, email, phone, password);
        } catch (error) {
            console.log(error);

            return_obj.status = false;
            return_obj.status_text = "Не удалось создать пользователя";
            return return_obj;
        }

        const data = await TokenService.parseAccessToken(user.access_token);

        try {
            products = JSON.stringify(products);

            await Orders.create({products, summ, delivery, address, comment, payment_method, user_id: data.user_id});
        } catch (error) {
            console.log(error);

            return_obj.status = false;
            return_obj.status_text = "Не удалось создать заказ";
        }

        return return_obj;
    }
}

module.exports = new OrdersService();