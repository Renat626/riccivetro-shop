const jwt = require("jsonwebtoken");
const jwt_decode = require("jwt-decode");

class TokenService {
    async generateTokens(data) {
        const accessToken = jwt.sign(data, process.env.JWT_ACCESS_SECRET, {expiresIn: "180s"});
        const refreshToken = jwt.sign(data, process.env.JWT_REFRESH_SECRET, {expiresIn: "30d"});
        return {
            accessToken,
            refreshToken
        }
    }

    async checkAccessToken(token) {
        try {
            const userData = jwt.verify(token, process.env.JWT_ACCESS_SECRET);
            return true;
        } catch (error) {
            return false;
        }
    }

    async parseAccessToken(token) {
        try {
            const userData = jwt_decode(token);
            console.log(userData);
            return userData;
        } catch (error) {
            return false;
        }
    }
}

module.exports = new TokenService();